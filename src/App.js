import React, { PureComponent } from 'react';
import Products from './components/Products'

import './assets/mixins/_index.scss';
import './assets/base/_index.scss';

class App extends PureComponent {
    render() {
        return (
            <Products />
        )
    }
}

export default App;
