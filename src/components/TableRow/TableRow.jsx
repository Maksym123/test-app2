import React from 'react';
import Checkbox from './../Checkbox';
import StarsRating from 'react-star-ratings';
import NoItems from './../NoItems';

import './style.scss';

const TableRow = ({ data }) => {

    return (
        (data.length === 0) ?
            <NoItems/>
            :
            data.map((data) => (
                <tr
                    key={ data.id }
                >
                    <td>
                        <Checkbox
                            checked={ false }
                            id={ `check${data.id}` }
                        />
                    </td>
                    <td>
                        { data.name }
                    </td>
                    <td>
                        <StarsRating
                            id={ data.id }
                            rating={ data.rating }
                            starRatedColor="#FFD700"
                            starDimension="20"
                            starSpacing="0"
                            numberOfStars={ 5 }
                        />

                    </td>
                    <td>
                        { data.price }
                    </td>
                </tr>
            ))
    );
};

export default TableRow;
