import React from 'react';

const NoItems = () => {
    return (
        <tr className="no-items">
            <td className="no-items-title">
                No items
            </td>
        </tr>
    )
};

export default NoItems;
