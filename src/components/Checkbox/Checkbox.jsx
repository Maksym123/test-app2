import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './style.scss';

class Checkbox extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            isChecked: this.props.checked
        };
    }

    toggleCheckBox = () => {
        if (this.props.onClick) {
            this.props.onClick(!this.state.isChecked, this.props.type);
        }

        this.setState({
            isChecked: !this.state.isChecked
        });
    };

    render() {
        console.log(this.props);

        return (
            <div className="checkbox">
                <input
                    type="checkbox"
                    id={this.props.id}
                    checked={ this.state.isChecked }
                    onChange={ this.toggleCheckBox }
                />

                <label className="checkbox-label" htmlFor={ this.props.id }><span>{ this.props.label }</span></label>
            </div>
        );
    }

    static propTypes = {
        checked: PropTypes.bool.isRequired,
        label: PropTypes.string,
        onClick: PropTypes.func,
        type: PropTypes.string
    };
}

export default Checkbox;
