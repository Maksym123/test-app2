import React, { PureComponent } from 'react';
import {findIndex } from 'lodash';
import CustomTable from './../../components/CustomTable';
import Checkbox from './../../components/Checkbox';
import data from './data/data.js';

// Styles.
import './style.scss';

class Products extends PureComponent {
    static PHONE = 'phone';
    static TABLET = 'tablet';
    static NOTEBOOK = 'notebook';

    constructor(props) {
        super(props);

        this.state = {
            dataForTable: []
        };

        this.selectedCategoty = [Products.PHONE];
    }

    componentWillMount() {

        // Prepare data on start loading.
        this.prepareDataForTable(data);
    }

    onCheck = (checked, type) => {
        if (checked) {
            this.selectedCategoty.push(type);
        }
        else {
            const checkboxIndex = findIndex(this.selectedCategoty,(item) => {
                return item === type;
            });

            if (checkboxIndex !== -1) {
                this.selectedCategoty.splice(checkboxIndex, 1);
            }
        }

        this.prepareDataForTable();
    };

    prepareDataForTable = () => {
        const filteredPosition = data.filter((item) => {
            return this.isCategorySelected(item.type);
        });

        this.setState(() => {
            return { dataForTable: filteredPosition };
        });
    };

    isCategorySelected = (type) => {
        return findIndex(this.selectedCategoty, (category) => {
            return category === type
        }) !== -1;
    };

    render() {
        return (
            <div className="wrapper">
                <div className="container">
                    <h1>Poducts</h1>

                    <div className="products">
                        <div className="row">
                            <div className="col-xs-24 col-sm-6">

                                <h2> Product filters</h2>

                                <div className="checkbox-filter">
                                    <Checkbox
                                        checked={ this.isCategorySelected(Products.PHONE) }
                                        label="Mobile phones"
                                        id="Mobile phones"
                                        type={ Products.PHONE }
                                        onClick={ this.onCheck }
                                    />
                                </div>

                                <div className="checkbox-filter">
                                    <Checkbox
                                        checked={ this.isCategorySelected(Products.TABLET) }
                                        label="Tablets"
                                        id="Tablets"
                                        type={ Products.TABLET }
                                        onClick={ this.onCheck }
                                    />
                                </div>

                                <div className="checkbox-filter">
                                    <Checkbox
                                        checked={ this.isCategorySelected(Products.NOTEBOOK) }
                                        label="Notebooks"
                                        id="Notebooks"
                                        type={ Products.NOTEBOOK }
                                        onClick={ this.onCheck }
                                    >
                                    </Checkbox>
                                </div>
                            </div>

                            <div className="col-xs-24 col-sm-18">
                                <div className="product-inner">
                                    <h2> Product table</h2>

                                    <CustomTable
                                        data={ JSON.stringify(this.state.dataForTable) }
                                    />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Products;
