const data = [{
    id: 1,
    type: "phone",
    name: "iPhone 8",
    price: "$1400",
    rating: 4.8
}, {
    id: 2,
    type: "phone",
    name: "Nokia 3310",
    price: "$10",
    rating: 2.1
}, {
    id: 3,
    type: "notebook",
    name: "Dell XPS NEW",
    price: "$2100",
    rating: 4.5
}, {
    id: 4,
    type: "notebook",
    name: "Asus Vivobook S15",
    price: "$800",
    rating: 3.7
}, {
    id: 5,
    type: "tablet",
    name: "Ipad 2",
    price: "$220",
    rating: 2
}, {
    id: 6,
    type: "tablet",
    name: "IPad 3",
    price: "$400",
    rating: 3
}];

export default data;
