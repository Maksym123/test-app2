import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import TableRow from './../TableRow';
import imgSrc from './images/check-icon.svg'

// Styles.
import './style.scss';

class CustomTable extends PureComponent {
    static ASC_SORT = 'ASC';
    static DESC_SORT = 'DESC';

    constructor(props) {
        super(props);

        this.state = {
            dataJSON: [],
            data: {},
            changeIconName: true,
            changeIconRating: true,
            changeIconPrice: true
        };
    }

    componentWillMount() {
        this.parseJSON();
    }

    componentWillReceiveProps(nextProps) {

        // Render data after filter is changed.
        this.setState({ data: nextProps.data });

        // Convert data  to json.
        setTimeout(() => this.parseJSON(), 50);
    }

    // Set new data to state
    parseJSON = () => {
        let data = [];

        const dataJSON = JSON.parse(this.props.data);

        for (let i = 0; i < dataJSON.length; ++i) {
            data.push(dataJSON[i]);
        }

        this.setState({
            dataJSON: data
        });

    };

    SortByPrice = () => {
        if (this.state.dataJSON.length > 1) {

            let data = this.state.dataJSON;

            if (!this.state.priceSortType || this.state.priceSortType === CustomTable.DESC_SORT) {
                data.sort((a, b) => {
                    const firstNum = parseFloat(a.price.slice(1), 10);
                    const secondNum = parseFloat(b.price.slice(1), 10);
                    return firstNum - secondNum;
                });

                this.setState({
                    priceSortType: CustomTable.ASC_SORT,
                    dataJSON: data,
                    changeIconPrice: false
                });

            } else {
                data.reverse();

                this.setState({
                    priceSortType: CustomTable.DESC_SORT,
                    dataJSON: data,
                    changeIconPrice: true
                });
            }
        }
    };

    SortByName = () => {
        if (this.state.dataJSON.length > 1) {

            let data = this.state.dataJSON;

            if (!this.state.nameSortType || this.state.nameSortType === CustomTable.DESC_SORT) {
                data.sort((a, b) => {
                    return a.name.localeCompare(b.name);
                });

                this.setState({
                    nameSortType: CustomTable.ASC_SORT,
                    dataJSON: data,
                    changeIconName: false
                });

            } else {
                data.reverse();

                this.setState({
                    nameSortType: CustomTable.DESC_SORT,
                    dataJSON: data,
                    changeIconName: true
                });
            }
        }
    };

    SortByRating = () => {
        if (this.state.dataJSON.length > 1) {

            let data = this.state.dataJSON;

            if (!this.state.ratingSortType || this.state.ratingSortType === CustomTable.DESC_SORT) {
                data.sort((a, b) => {
                    const firstNum = parseFloat(a.rating);
                    const secondNum = parseFloat(b.rating);
                    return firstNum - secondNum;
                });

                this.setState({
                    ratingSortType: CustomTable.ASC_SORT,
                    dataJSON: data,
                    changeIconRating: false
                });

            } else {
                data.reverse();

                this.setState({
                    ratingSortType: CustomTable.DESC_SORT,
                    dataJSON: data,
                    changeIconRating: true
                });
            }
        }
    };

    render() {
        const {
            changeIconName,
            changeIconRating,
            changeIconPrice
        } = this.state;

        return (
            <div className="product-table-wrapper">
                <table className="product-table">
                    <thead>
                        <tr>
                            <th>
                                <img
                                    width='24px'
                                    height="24px"
                                    src={imgSrc}
                                    alt="Check item"
                                />
                            </th>
                            <th onClick={ this.SortByName }>
                                <span>Product name
                                <span
                                >{
                                    changeIconName
                                        ? <span className="table-icon">&#8897;</span>
                                        : <span className="table-icon">&#8896;</span>
                                }
                                </span>
                                </span>
                            </th>
                            <th onClick={ this.SortByRating }>
                                   <span>Rating
                                <span
                                >{
                                    changeIconRating
                                        ? <span className="table-icon">&#8897;</span>
                                        : <span className="table-icon">&#8896;</span>
                                }
                                </span>
                                </span>

                            </th>
                            <th onClick={ this.SortByPrice }>
                                <span>Price
                                <span
                                >{
                                    changeIconPrice
                                        ? <span className="table-icon">&#8897;</span>
                                        : <span className="table-icon">&#8896;</span>
                                }
                                </span>
                                </span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <TableRow data={ this.state.dataJSON }/>
                    </tbody>
                </table>
            </div>
        );
    }

    static propTypes = {
        data: PropTypes.string.isRequired
    };
}

export default CustomTable;
